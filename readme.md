## ts急速入门

学习ts语言入门，参考b站 BV1yt411e7xV

#### 一、类型

- ​    布尔类型        `boolean`
- ​    数字类型        `number`
- ​    字符串类型      `string`
- ​    数组类型        `number[]、Array<number>`
- ​    元组类型        `let arr5:[string, number, boolean] = ["ts", 12, false]`
- ​    枚举类型        `enum Flag {success=1, error=-1}、enum Color {blue, yellow, green}`
- ​    任意类型        `any`
- ​    undefine       `undefine`
- ​    多重类型        `var num2:number | undefined`

#### 二、方法

- ​    声明函数        `function run():void{}`

- ​    匿名函数        `var fun2 = function():number{}`

- ​    函数参数        `function getInfo(name:string, age:number:string) {}`

- ​    可选参数        `function getInfo2(name:string, age?:number):string {}`

- ​    默认参数        `function getInfo3(name:string, age:number=18):string {}`

- ​    变长参数        `function sum(sum:number, ...result:number[]):number {}`\

  

- ​	函数重载

- ​	箭头函数        `setTimeout(() => {console.log('箭头函数')}, 1000)`

#### 三、类

- ​    类的定义        				    `class`
- ​    类的继承         				   `extends、super`
- ​    类属性的修饰符    	   	 `public、protected、private`
- ​    静态属性、静态方法		 `static`
- ​    多态
  ​    抽象类、抽象方法   		 `abstract`

#### 四、接口

- ​    属性类接口
  ​        1>对json的约束 类似string、number
  ​        2>接口：可选属性
- ​    函数类型接口    对方法传入的参数和返回值进行约束
- ​    可索引接口      数组对象的约束 不常用
- ​    类类型接口      
- ​    接口扩展        接口继承接口

#### 五、泛型

 any 放弃类型检查， 泛型可以规范传入类型和返回类型

- ​    泛型方法
- ​    泛型类
- ​    泛型接口
- ​    把类作为参数类型的泛型接口

#### 六、定义一个操作数据库的库  代码样例

#### 七、模块化      export import

#### 八、命名空间    namespace

#### 九、装饰器

- ​    类装饰器

- ​    属性装饰器

- ​    方法装饰器

- ​    方法参数装饰器

- ​    装饰器执行顺序：从里到外，从上到下，从右到左

  

```markdown
// 安装： npm install -g typescript
// 配置1：ts.config.json文件     修改属性"outDir": "./js",  
// 配置2：终端->运行任务->typescripe->tsc监视
// 运行：ts helloworld.ts
```

