namespace A {
    export abstract class Animal {
        name:string;
        constructor(name:string) {
            this.name = name;
        }
    
        abstract eat():void;
    }
    
    export class Dog extends Animal {
        constructor(name:string) {
            super(name)
        }
    
        eat() {
            console.log(`${this.name}吃粑粑`)
        }
    }
    
    export class Cat extends Animal {
        constructor(name:string) {
            super(name)
        }
    
        eat() {
            console.log(`${this.name}吃猫粮`)
        }
    }
}



namespace B {
    export abstract class Animal {
        name:string;
        constructor(name:string) {
            this.name = name;
        }
    
        abstract eat():void;
    }
    
    export class Dog extends Animal {
        constructor(name:string) {
            super(name)
        }
    
        eat() {
            console.log(`${this.name}吃粑粑`)
        }
    }
    
    export class Cat extends Animal {
        constructor(name:string) {
            super(name)
        }
    
        eat() {
            console.log(`${this.name}吃猫粮`)
        }
    }
}

export {A, B}