interface DBI<T> {
    add(info:T):boolean;
    update(info:T, id:number):boolean;
    delete(id:number):boolean;
    get(id:number):any[];
}

class MysqlDb<T> implements DBI<T> {

    add(info: T): boolean {
        console.log(info)
        return true;  
    }
    update(info: T, id: number): boolean {
        throw new Error("Method not implemented.")
    }
    delete(id: number): boolean {
        throw new Error("Method not implemented.")
    }
    get(id: number): any[] {
        throw new Error("Method not implemented.")
    }
}

class MsSqlDb<T> implements DBI<T> {

    add(info: T): boolean {
        console.log(info);
    
        return true;
    }
    update(info: T, id: number): boolean {
        throw new Error("Method not implemented.")
    }
    delete(id: number): boolean {
        throw new Error("Method not implemented.")
    }
    get(id: number): any[] {
        throw new Error("Method not implemented.")
    }

}

export {MysqlDb, MsSqlDb}